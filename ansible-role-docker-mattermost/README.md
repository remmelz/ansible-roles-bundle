
Ansible Role Docker Mattermost
===============================

Mattermost is an open-source, self-hostable online chat service with file
sharing, search, and integrations. It is designed as an internal chat for
organisations and companies, and mostly markets itself as an open-source
alternative to Slack and Microsoft Teams.

Requirements
------------

No pre-requisites.


Role Variables
--------------

A description of the settable variables for this role should go here, including
any variables that are in defaults/main.yml, vars/main.yml, and any variables
that can/should be set via parameters to the role. Any variables that are read
from other roles and/or the global scope (ie. hostvars, group vars, etc.)
should be mentioned here as well.

| Variable                | Required | Default | Comments                                 |
|-------------------------|----------|---------|------------------------------------------|
| foo                     | no       | false   | example variable                         |
| bar                     | yes      |         | example variable                         |

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in
regards to parameters that may need to be set for other roles, or variables
that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: all
      roles:
         - ansible-role-docker-mattermost

License
-------

See license.md



