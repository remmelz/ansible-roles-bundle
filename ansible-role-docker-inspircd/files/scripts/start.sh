#!/bin/bash

##############################
# Start Inspircd
##############################

while true; do

  printf "Checking inspircd daemon.."
  if [[ `ps aux | grep 'inspircd' \
        | wc -l` -le 1 ]]; then

    echo "[Down]"
    printf "Starting inspircd server..."
    su irc -c '/usr/sbin/inspircd'
    sleep 1
    /etc/init.d/anope start
  fi

  echo "[Ok]"
  sleep 10
done

