#!/bin/bash

##############################
# Start some service
##############################

while true; do

  printf "Checking some daemon.."
  if [[ `ps aux | grep 'some_daemon' \
        | wc -l` -le 1 ]]; then

    echo "[Down]"
    printf "Starting some service..."
    #/etc/init.d/some_service start
    sleep 1
  fi

  echo "[Ok]"
  sleep 10
done

