
Ansible Role Docker InspIRCd
==================

InspIRCd is a modular Internet Relay Chat (IRC) server written in C++ for
Linux, BSD, Windows and macOS systems.

* https://www.inspircd.org/


Requirements
------------

No pre-requisites.


Role Variables
--------------

A description of the settable variables for this role should go here, including
any variables that are in defaults/main.yml, vars/main.yml, and any variables
that can/should be set via parameters to the role. Any variables that are read
from other roles and/or the global scope (ie. hostvars, group vars, etc.)
should be mentioned here as well.

| Variable                | Required | Default | Comments                                 |
|-------------------------|----------|---------|------------------------------------------|
| server_port             | no       | false   | port to connect to server                |
| connect_password        | yes      | secret  | password to connect to server            |


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in
regards to parameters that may need to be set for other roles, or variables
that are used from other roles.


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: all
      roles:
         - ansible-role-template

License
-------

See license.md



