#!/bin/bash

###################
apt-get -y update && \
apt-get -y install \
  procps \
  bepasty && \
###################
useradd pasty && \
mkdir -p /srv/bepasty/storage && \
cp -v /var/tmp/bepasty.conf /srv/bepasty && \
chown -Rv pasty. /srv/bepasty && \
###################
cp -v /var/tmp/style.css /usr/lib/python3/dist-packages/bepasty/static/app/css/style.css && \
exit

