#!/bin/bash

sleep 1

CONF="/srv/bepasty/bepasty.conf"

while true; do

  su - pasty -c "BEPASTY_CONFIG=${CONF} bepasty-server --host 0.0.0.0 --port 5000"

  sleep 10
done

