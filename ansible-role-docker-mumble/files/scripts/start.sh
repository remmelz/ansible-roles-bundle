#!/bin/bash

##############################
# Start Mumble
##############################

while true; do

  printf "Checking mumble daemon.."
  if [[ `ps aux | grep 'mumble' \
        | wc -l` -lt 1 ]]; then

    echo "[Down]"
    printf "Starting mumble server..."
    service mumble-server start
  fi

  echo "[Ok]"
  sleep 10
done


