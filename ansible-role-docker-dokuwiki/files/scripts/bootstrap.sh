#!/bin/bash

###################
apt-get -y update && \
apt-get -y install \
  wget \
  nginx \
  php \
  php-gd \
  php-fpm \
  php-xml && \
mkdir -p /var/run/php && \
chown www-data. /var/run/php && \
cd /var/run/php && \
ln -s php7.3-fpm.sock php-fpm.sock && \
###################
cd /srv && \
###################
wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz && \
tar xzf dokuwiki-stable.tgz && \
rm *.tgz && \
mv * dokuwiki && \
chown -R www-data. * && \
###################
mv -v /var/tmp/nginx.conf /etc/nginx/sites-available/dokuwiki && \
cd /etc/nginx/sites-enabled && \
rm default && \
ln -s ../sites-available/dokuwiki
