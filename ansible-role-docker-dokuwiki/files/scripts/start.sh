#!/bin/bash

service php7.3-fpm start
service nginx start

sleep 1

while true; do

  if [[ ! -f /srv/installed ]]; then
    if [[ -f /srv/dokuwiki/conf/users.auth.php ]]; then

      sed -i 's/##loc/loc/g' /etc/nginx/sites-enabled/dokuwiki
      service nginx restart
      touch /srv/installed

    else
      echo 'Warning: DokuWiki not configured yet! Use ../install.php to configure.'
    fi
  fi

  service nginx status

  sleep 10


done

