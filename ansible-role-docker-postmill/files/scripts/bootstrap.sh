#!/bin/bash

###################
apt-get -y update && \
apt-get -y install \
  git \
  yarnpkg \
  composer \
  php-curl \
  php-gd \
  php-iconv \
  php-intl \
  php-json \
  php-mbstring \
  php-tokenizer \
  php-pgsql \
  php-xml && \
###################
useradd -d /srv/postmill postmill && \
cd /srv && \
git clone https://gitlab.com/postmill/Postmill.git && \
# Set colors top-bar to blue
sed -i 's/#e10/#005390/g' ./Postmill/assets/css/themes/postmill/_light.less && \
sed -i 's/#c00/#005390/g' ./Postmill/assets/css/themes/postmill/_light.less && \
sed -i 's/#a00/#023f6c/g' ./Postmill/assets/css/themes/postmill/_light.less && \
mv Postmill postmill && \
cd postmill && \
yarnpkg install && \
yarnpkg run build-prod && \
composer install && \
mv /var/tmp/env.txt /srv/postmill/.env.local && \
mkdir data && \
chown -R postmill. /srv/postmill && \
###################
exit
