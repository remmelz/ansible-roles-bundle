#!/bin/bash

##############################
# Start Postmill
##############################

su - postmill -c 'bin/console -n doctrine:migrations:migrate'
su - postmill -c 'bin/console -n postmill:admin admin'

while true; do
  su - postmill -c 'php -S 0.0.0.0:8000 -t public'
  sleep 10
done

