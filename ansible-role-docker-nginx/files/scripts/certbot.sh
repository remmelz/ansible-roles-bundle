#!/bin/bash

##############################
# Start receiving certs
##############################

cd /etc/nginx/conf.d/

for conf in `ls -1`; do

  [[ -z `grep server_name ${conf}` ]] && continue

  dom=`grep server_name ${conf} \
    | awk -F' ' '{print $2}' \
    | sed 's/;//g' \
    | sort -u`

  [[ ${dom} == "localhost" ]] && continue
  
  testcert="--test-cert"
  if [[ -z `grep -- ${testcert} ${conf}` ]]; then
    testcert=""
  fi

  if [[ -d /etc/letsencrypt/live/${dom} ]]; then
    echo "Certificate for ${dom} already received."
    continue
  fi

  if [[ -n `echo ${snakeoil} | grep -i true` ]]; then
    mkdir -p /etc/letsencrypt/live/${dom}
    cp -v /etc/ssl/private/ssl-cert-snakeoil.key /etc/letsencrypt/live/${dom}/privkey.pem
    cp -v /etc/ssl/certs/ssl-cert-snakeoil.pem   /etc/letsencrypt/live/${dom}/fullchain.pem
    continue
  fi
  
  set -x
  certbot certonly -n --standalone \
    ${xargs} ${testcert} -d ${dom} \
    -m ${email} \
    --agree-tos
  set +x

  [[ $? != 0 ]] && exit 1

done


