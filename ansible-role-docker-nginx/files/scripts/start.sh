#!/bin/bash

##############################
# Variables
##############################

email=${EMAIL_TO}
xargs="${EXTRA_ARGS}"
snakeoil=${SNAKEOIL_CERTS}

##############################
# Start receiving certs
##############################

. /scripts/certbot.sh

##############################
# Start Nginx
##############################

counter=0
while true; do

  # Check renew certificate every 10sec*
  if [[ ${counter} -ge 10000 ]] || \
     [[ ${counter} -eq 0 ]]; then
    service nginx stop
    certbot renew
    counter=1
  fi

  service nginx status
  [[ $? != 0 ]] && service nginx start
  sleep 10

  let counter=${counter}+1
done



